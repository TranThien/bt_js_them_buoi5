function thuNhapChiuThueTu0Den60Tr() {
    return 0.05;
}

function thuNhapChiuThueTu61Den120Tr() {
    return 0.1;
}

function thuNhapChiuThueTu121Den210Tr() {
    return 0.15;
}

function thuNhapChiuThueTu211Den384Tr() {
    return 0.2;
}

function thuNhapChiuThueTu385Den624Tr() {
    return 0.25;
}

function thuNhapChiuThueTu625Den960Tr() {
    return 0.3;
}

function thuNhapChiuThueTu961Tr() {
    return 0.35;
}

function showResult() {
    var nameElement = document.getElementById("name").value;
    var tongThuNhap = document.getElementById("total-year").value * 1;
    var nguoiPhuThuoc = document.getElementById("person").value * 1;
    var thuNhapChiuThue = tongThuNhap - 4e6 - nguoiPhuThuoc * 1.6e6;
    var tinhThuNhapChiuThueTu0Den60Tr = thuNhapChiuThueTu0Den60Tr(tongThuNhap);
    var tinhThuNhapChiuThueTu61Den120Tr =
        thuNhapChiuThueTu61Den120Tr(tongThuNhap);
    var tinhThuNhapChiuThueTu121Den210Tr =
        thuNhapChiuThueTu121Den210Tr(tongThuNhap);
    var tinhThuNhapChiuThueTu211Den384Tr =
        thuNhapChiuThueTu211Den384Tr(tongThuNhap);
    var tinhThuNhapChiuThueTu385Den624Tr =
        thuNhapChiuThueTu385Den624Tr(tongThuNhap);
    var tinhThuNhapChiuThueTu625Den960Tr =
        thuNhapChiuThueTu625Den960Tr(tongThuNhap);
    var tinhThuNhapChiuThueTu961Tr = thuNhapChiuThueTu961Tr(tongThuNhap);
    var thuNhapConLai = 0;
    if (tongThuNhap <= 60e6) {
        thuNhapConLai = thuNhapChiuThue * tinhThuNhapChiuThueTu0Den60Tr;
    } else if (tongThuNhap > 60e6 && tongThuNhap <= 120e6) {
        thuNhapConLai =
            60e6 * tinhThuNhapChiuThueTu0Den60Tr +
            (thuNhapChiuThue - 60e6) * tinhThuNhapChiuThueTu61Den120Tr;
        // thuNhapChiuThue = thuNhapChiuThue * tinhThuNhapChiuThueTu61Den120Tr;
    } else if (tongThuNhap > 120e6 && tongThuNhap <= 210e6) {
        thuNhapConLai =
            60e6 * tinhThuNhapChiuThueTu0Den60Tr +
            60e6 * tinhThuNhapChiuThueTu61Den120Tr +
            (thuNhapChiuThue - 120e6) * tinhThuNhapChiuThueTu121Den210Tr;
        // thuNhapChiuThue = thuNhapChiuThue * tinhThuNhapChiuThueTu121Den210Tr;
    } else if (tongThuNhap > 210e6 && tongThuNhap <= 384e6) {
        thuNhapConLai =
            60e6 * tinhThuNhapChiuThueTu0Den60Tr +
            60e6 * tinhThuNhapChiuThueTu61Den120Tr +
            90e6 * tinhThuNhapChiuThueTu121Den210Tr +
            (thuNhapChiuThue - 210e6) * tinhThuNhapChiuThueTu211Den384Tr;
        // thuNhapChiuThue = thuNhapChiuThue * tinhThuNhapChiuThueTu211Den384Tr;
    } else if (tongThuNhap > 384e6 && tongThuNhap <= 624e6) {
        thuNhapConLai =
            60e6 * tinhThuNhapChiuThueTu0Den60Tr +
            60e6 * tinhThuNhapChiuThueTu61Den120Tr +
            90e6 * tinhThuNhapChiuThueTu121Den210Tr +
            174e6 * tinhThuNhapChiuThueTu211Den384Tr +
            (thuNhapChiuThue - 384e6) * tinhThuNhapChiuThueTu385Den624Tr;
        // thuNhapChiuThue = thuNhapChiuThue * tinhThuNhapChiuThueTu385Den624Tr;
    } else if (tongThuNhap > 624e6 && tongThuNhap <= 960e6) {
        thuNhapConLai =
            60e6 * tinhThuNhapChiuThueTu0Den60Tr +
            60e6 * tinhThuNhapChiuThueTu61Den120Tr +
            90e6 * tinhThuNhapChiuThueTu121Den210Tr +
            174e6 * tinhThuNhapChiuThueTu211Den384Tr +
            240e6 * tinhThuNhapChiuThueTu385Den624Tr +
            (thuNhapChiuThue - 624e6) * tinhThuNhapChiuThueTu625Den960Tr;
        // thuNhapChiuThue = thuNhapChiuThue * tinhThuNhapChiuThueTu625Den960Tr;
    } else {
        thuNhapConLai =
            60e6 * tinhThuNhapChiuThueTu0Den60Tr +
            60e6 * tinhThuNhapChiuThueTu61Den120Tr +
            90e6 * tinhThuNhapChiuThueTu121Den210Tr +
            174e6 * tinhThuNhapChiuThueTu211Den384Tr +
            240e6 * tinhThuNhapChiuThueTu385Den624Tr +
            336e6 * tinhThuNhapChiuThueTu625Den960Tr +
            (thuNhapChiuThue - 960e6) * tinhThuNhapChiuThueTu961Tr;
        // thuNhapChiuThue = thuNhapChiuThue * tinhThuNhapChiuThueTu961Tr;
    }
    document.querySelector(
        "#result"
    ).innerHTML = `Họ và tên : ${nameElement} , Tiền thuế thu nhập cá nhân cần phải đóng là: ${Intl.NumberFormat().format(
    thuNhapConLai
  )} VNĐ`;
}

//

function myFunction() {
    var displayElement = document.querySelector("#ddlViewBy").value;
    if (displayElement == "Doanh Nghiep") {
        var hideElement = document.getElementById("hide");

        hideElement.classList.add("active");
    } else {
        document.querySelector(".form-group-1.active").classList.remove("active");
    }
}

function phiXuLyHoaDon() {
    var displayElement = document.querySelector("#ddlViewBy").value;

    if (displayElement == "Gia Dinh") {
        return 4.5;
    } else if (displayElement == "Doanh Nghiep") {
        return 15;
    } else {
        return undefined;
    }
}

function phiDichVuCoBanNhaDan() {
    var channelElement = document.getElementById("channel").value * 1;
    var displayElement = document.querySelector("#ddlViewBy").value;
    if (displayElement == "Gia Dinh" && channelElement >= 1) {
        return 20.5;
    } else {
        return undefined;
    }
}

function phiDichVuCoBanDoanhNghiep() {
    var channelElement = document.getElementById("channel").value * 1;
    var displayElement = document.querySelector("#ddlViewBy").value;
    var connectionElement = document.querySelector("#connection").value * 1;
    if (displayElement == "Doanh Nghiep" && connectionElement <= 10) {
        return 75;
    } else if (displayElement == "Doanh Nghiep" && connectionElement > 10) {
        return 75 + (connectionElement - 10) * 5;
    } else {
        return undefined;
    }
}

function phiThueKenhCaoCap() {
    var displayElement = document.querySelector("#ddlViewBy").value;

    if (displayElement == "Gia Dinh") {
        return 7.5;
    } else if (displayElement == "Doanh Nghiep") {
        return 50;
    } else {
        return undefined;
    }
}

function tinhTien() {
    var customElement = document.querySelector("#customer").value;
    var channelElement = document.getElementById("channel").value * 1;
    var connectionElement = document.querySelector("#connection").value * 1;
    var displayElement = document.querySelector("#ddlViewBy").value;
    var tinhPhiXuLyHoaDon = phiXuLyHoaDon();
    var tinhPhiDichVuCoBanNhaDan = phiDichVuCoBanNhaDan();
    var tinhPhiDichVuCoBanDoanhNghiep = phiDichVuCoBanDoanhNghiep();
    var tinhphiThueKenhCaoCap = phiThueKenhCaoCap();
    total = 0;
    if (displayElement == "Doanh Nghiep") {
        total =
            tinhPhiXuLyHoaDon +
            tinhPhiDichVuCoBanDoanhNghiep +
            tinhphiThueKenhCaoCap * channelElement;
    } else if (displayElement == "Gia Dinh") {
        total =
            tinhPhiXuLyHoaDon +
            tinhPhiDichVuCoBanNhaDan +
            tinhphiThueKenhCaoCap * channelElement;
    } else {
        return undefined;
    }

    document.querySelector(
        "#content"
    ).innerHTML = `Mã Khách Hàng : ${customElement} , Tiền cáp : $ ${total}`;
}